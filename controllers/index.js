var foods = [
    { id: 1, name: 'Bún Bò', price: 100, imageUrl: 'https://trivietphat.net/wp-content/uploads/2021/08/bun-bo-1.jpg' },
    { id: 2, name: 'Bánh Canh Cua', price: 200, imageUrl: 'https://www.hoidaubepaau.com/wp-content/uploads/2015/12/banh-canh-bot-loc.jpg' },
    { id: 3, name: 'Phở', price: 300, imageUrl: 'https://www.gbni.co.jp/content/img/2021/08/49ef72b19c9b2addea8db508ca9b00b7-1.jpg' },
]

document.querySelector(`#btn-add-food`).addEventListener('click', function () {
    var food = new Food();
    food.id = document.querySelector(`#food-id`).value;
    food.name = document.querySelector(`#food-name`).value;
    food.price = document.querySelector(`#food-price`).value;
    food.imageUrl = Number(document.querySelector(`#food-image-url`).value);    
    foods.push(food);
    saveToLocalStorage();

    renderTable(foods);
});

function removeFood(id) {
    for (var index = 0; index < foods.length; index++) {
        var food = foods[index];
        if (food.id === id) {
            foods.splice(index, 1);
        }

        renderTable(foods);
    }
}

function renderTable(foods) {
    var innerHtmlContent = ``;
    for (var index = 0; index < foods.length; index++) {
        var food = foods[index];

        innerHtmlContent += `
                    <tr>
                        <td>${food.id}</td>
                        <td>${food.name}</td>
                        <td>${food.price}</td>
                        <td>
                            <img style="width: 200px;height:150px;" src="${food.imageUrl}"/>
                        </td>
                        <td>
                            <button class="btn btn-danger" id="btn-remove-cart-item" onclick="removeFood('${food.id}')">Xoá</button>
                        </td>
                    </tr>`;
    }

    document.querySelector(`#food-data`).innerHTML = innerHtmlContent;
    return innerHtmlContent;
}

function saveToLocalStorage() {
    var foods = JSON.stringify(foods);
    localStorage.setItem(`foods`, foods);
}

function getDataFromLocalStorage() {
    if (localStorage.getItem(foods)) {
        var foods = localStorage.getItem(`foods`);
        foods = JSON.parse(foods);

        renderTable(foods);
    }
}

getDataFromLocalStorage();